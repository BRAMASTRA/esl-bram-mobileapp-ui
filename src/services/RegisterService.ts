import { Component, Injectable } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';


@Injectable()
export class RegisterService {
    BASE_URL="http://52.66.150.242";

    //BASE_URL="http://10.0.2.122";
    
    

    
  constructor(  public http: Http){
  
  }

  registerFunction(data: any)  {

    let headers = new Headers ({ 'Content-Type': 'application/json',
    'tenantIdHeader': 0
  });

  let options = new RequestOptions({ headers: headers, method: "post" });
 return this.http.post(this.BASE_URL +'/userSignup' , data, options);
        }
  
}