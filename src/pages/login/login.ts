import {Component} from "@angular/core";
import {NavController, AlertController, ToastController, MenuController} from "ionic-angular";
import {HomePage} from "../home/home";
import {RegisterPage} from "../register/register";
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { LoginService } from '../../services/LoginService';



@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {



  validations_form: FormGroup;

  constructor(public nav: NavController, public forgotCtrl: AlertController,  public loginService: LoginService,    public formBuilder: FormBuilder,
    public menu: MenuController, public toastCtrl: ToastController) {
    this.menu.swipeEnable(false);
  }



  ionViewWillLoad() {

        this.validations_form = this.formBuilder.group({
          sEmailIdOrnMobileNo: new FormControl('', Validators.compose([
            Validators.maxLength(25),
            Validators.minLength(2),
            Validators.required
          ])),



        });


      }

  // go to register page
  register() {
    this.nav.setRoot(RegisterPage);
  }

  // login and go to home page
  login(value: string): void{

    if(value == ""){
      console.log("data",value);

    }else{
      this.loginService.loginFunction(value)

        .subscribe(res => {
          console.log(res.json());
          var responseMessage = res.json();
          localStorage.setItem("response",JSON.stringify(responseMessage.data));
          localStorage.setItem("jwttoken",responseMessage.data.jwttoken);

          if (responseMessage.success == true) {

          } else {

          }
        }, (err) => {

          var error = JSON.parse(err._body);
          var errMsg = error.message;
          // let alert = this.alertCtrl.create({
          //   subTitle: errMsg,
          //   buttons: ['OK']
          // });
          // alert.present();
        });

      console.log("data",value);


      this.nav.setRoot(HomePage);

    }

  }

  forgotPass() {
    let forgot = this.forgotCtrl.create({
      title: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
            let toast = this.toastCtrl.create({
              message: 'Email was sended successfully',
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'OK',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

}
