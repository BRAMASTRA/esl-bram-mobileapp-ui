import {Component} from "@angular/core";
import {NavController, PopoverController} from "ionic-angular";
import {Storage} from '@ionic/storage';

import {NotificationsPage} from "../notifications/notifications";
import {SettingsPage} from "../settings/settings";
import {TripsPage} from "../trips/trips";
import {SearchLocationPage} from "../search-location/search-location";
import {AlertPage} from "../alert/alert";

import { AdviceAlert } from '../../services/AdviceAlert';

@Component({
  selector: 'page-home',
  templateUrl: 'showalert.html'
})

export class ShowAlertPage {
  // search condition
  public search = {
    name: "Rio de Janeiro, Brazil",
    date: new Date().toISOString()
  }

  constructor(private storage: Storage, public nav: NavController, public adviceAlert: AdviceAlert,public popoverCtrl: PopoverController) {
  }

  ionViewWillEnter() {
    // this.search.pickup = "Rio de Janeiro, Brazil";
    // this.search.dropOff = "Same as pickup";
    this.storage.get('pickup').then((val) => {
      if (val === null) {
        this.search.name = "Rio de Janeiro, Brazil"
      } else {
        this.search.name = val;
      }
    }).catch((err) => {
      console.log(err)
    });
  }

  // go to result page
  doSearch() {
    this.nav.push(TripsPage);
  }

  // choose place
  choosePlace(from) {
    this.nav.push(SearchLocationPage, from);
  }

  // to go account page
  goToAccount() {
    this.nav.push(SettingsPage);
  }

  presentNotifications(myEvent) {
    console.log(myEvent);
    let popover = this.popoverCtrl.create(NotificationsPage);
    popover.present({
      ev: myEvent
    });
  }


  addAlert(){


    this.nav.push(AlertPage);
    
    
  }

  save(value: string): void{

    this.adviceAlert.save(value)
    
            .subscribe(res => {
              console.log(res.json());
              var responseMessage = res.json();
              localStorage.setItem("response",JSON.stringify(responseMessage.data));
              localStorage.setItem("jwttoken",responseMessage.data.jwttoken);
    
              if (responseMessage.success == true) {
    
              } else {
    
              }
            }, (err) => {
    
              var error = JSON.parse(err._body);
              var errMsg = error.message;
              // let alert = this.alertCtrl.create({
              //   subTitle: errMsg,
              //   buttons: ['OK']
              // });
              // alert.present();
            });
    
          console.log("data",value);
    
    
    
        }

}

//
